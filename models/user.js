const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const Env = require('../config/env');

const { Schema } = mongoose;

const UserSchema = new Schema({
    login: {
        type: String,
        required: true,
    },
    hash: {
        type: String,
        select: false,
    },
    salt: {
        type: String,
        select: false,
    },
}, {
    timestamps: true
});

UserSchema.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

UserSchema.methods.validatePassword = function (password) {
    const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash === hash;
};

UserSchema.methods.generateJWT = function () {
    const today = new Date();
    const expirationDate = new Date(today);
    expirationDate.setDate(today.getDate() + 60);

    return jwt.sign({
        _id: this._id,
        login: this.login,
        exp: parseInt(expirationDate.getTime() / 1000, 10),
    }, Env.SECRET);
};

const User = mongoose.model('User', UserSchema);

module.exports = User;

const User = require('../models/user');

const userService = {
    async createUser(user) {
        await validateUser(user);

        const userToSave = new User(user);

        userToSave.setPassword(user.password);

        try {
            const savedUser = await userToSave.save();
            return {
                login: savedUser.login,
            };
        } catch (error) {
            throw error;
        }
    },
};

async function validateUser(user) {
    if (!user) {
        const err = new Error('Empty Object');
        err.status = 400;

        throw err;
    }

    if (!user.login) {
        const err = new Error('Login é obrigatório');
        err.status = 400;

        throw err;
    }

    try {
        await validateAlreadyRegisteredLogin(user.email);
    } catch (error) {
        throw error;
    }

    if (!user.password) {
        const err = new Error('Senha é obrigatório');
        err.status = 400;

        throw err;
    }
}

async function validateAlreadyRegisteredLogin(login) {
    const userLogin = await User.findOne({login}).exec();

    if (userLogin) {
        const err = new Error('Este login já foi cadastrado');
        err.status = 400;

        throw err;
    }
}

module.exports = userService;

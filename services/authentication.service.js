const User = require('../models/user');
const authorizationService = require('./authorization.service');

const loginService = {
    async login(login, password) {
        try {
            validateLogin({ login, password });
            const fields = {
                _id: true,
                login: true,
                salt: true,
                hash: true
            };

            const user = await User.findOne({ login }).select(fields).exec();

            if (!user || !user.validatePassword(password)) {
                const err = new Error('Login ou senha inválido');
                err.status = 400;
                throw err;
            }

            const token = user.generateJWT();
            authorizationService.insertUser(token, user._id.toString());

            return {
                token,
            };
        } catch (error) {
            throw error;
        }
    },
    logout(token) {
        if (!token) {
            throw new Error('Nenhum usuário encontrado');
        }

        return authorizationService.removeUserToken(token);
    }
};

function validateLogin(loginObj) {
    if (loginObj) {
        if (!loginObj.login) {
            const err = new Error('Login é obrigatório');
            err.status = 400;

            throw err;
        }

        if (!loginObj.password) {
            const err = new Error('Senha é obrigatório');
            err.status = 400;

            throw err;
        }
    } else {
        const err = new Error('Login e senha são obrigatórios');
        err.status = 400;

        throw err;
    }
}

module.exports = loginService;

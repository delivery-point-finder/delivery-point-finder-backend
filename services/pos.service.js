const fs = require('fs');
const axios = require('axios');
const { Parser } = require('json2csv');
const {PEGAKI_API_KEY: pegakiApiKey, PEGAKI_API_URL: pegakiApiUrl} = require('../config/env');
const headers = {
    'Authorization': pegakiApiKey,
    'X-Total-Limit': 10
};
const apiUrl = `${pegakiApiUrl}/pontos/`;

const csvHeaders = [
    'cep_referencia',
    'id',
    'nome_fantasia',
    'endereco',
    'numero',
    'complemento',
    'bairro',
    'cep',
    'cidade',
    'estado',
    'telefone',
    'latitude',
    'longitude',
    'foto',
    'distancia',
    'horario_funcionamento',
    'features'
];

const lineBreak = '\r\n';
const cepRegex = /^[0-9]{8}$/;

const json2csvParser = new Parser({
    header: false,
    transforms: [
        transformRawData,
    ]
});

const posService = {
    async getNearPosByCEP(cep) {
        const apiUrlWithCEP = `${apiUrl}${cep}`;

        try {
            const response = await axios.get(apiUrlWithCEP, {headers});

            return response.data;
        } catch (error) {
            const errorObj = new Error();
            errorObj.status = error.response.status;
            errorObj.message = error.response.data.message

            throw errorObj;
        }
    },
    async listOfNearPosByCEPs(cepsListFile) {
        try {
            const cepsList = await fs.readFileSync(cepsListFile.path)
                .toString()
                .split('\n');

            const listOfPromises = cepsList.filter((cep) => {
                    return !!cep;
                }).map(async cep => {
                    if (!cepRegex.test(cep)) {
                        return {
                            [cep]: ['CEP inválido']
                        };
                    }

                    const apiUrlWithCEP = `${apiUrl}${cep}`;

                    try {
                        console.log('request:', apiUrlWithCEP);
                        const response = await axios.get(apiUrlWithCEP, {headers});
                        return {
                            [cep]: response.data
                        };
                    } catch (error) {
                        return {
                            [cep]: [error.response.data]
                        };
                    }
                });

            const listOfNearPosByCEPs = await Promise.all(listOfPromises);

            let csvLines = `${csvHeaders.join()} ${lineBreak}`;

            for (let p = 0; p < listOfNearPosByCEPs.length; p++) {
                const cepObj = listOfNearPosByCEPs[p];
                const cep = Object.keys(cepObj)[0];
                const posList = cepObj[cep].results || cepObj[cep];

                if (posList.length > 0) {
                    for (let i = 0; i < posList.length; i++) {
                        csvLines+=`${cep},${buildPosString(posList[i])} ${lineBreak}`;
                    }
                }
            }

            fs.unlinkSync(`${cepsListFile.path}`);
            return Buffer.from(csvLines);
        } catch (error) {
            fs.unlinkSync(`${cepsListFile.path}`);

            const errorObj = new Error();
            errorObj.status = error.response.status;
            errorObj.message = error.response.data.message

            throw errorObj;
        }

    }
}

function buildPosString(pos) {
    if (typeof pos === 'string') {
        return pos;
    }

    const parsed = json2csvParser.parse(pos);

    return parsed;
}

function transformRawData(pos) {
    if(pos.horario_funcionamento) {
        pos.horario_funcionamento = buildWorkingHoursString(pos.horario_funcionamento);
    }

    if (pos.features) {
        pos.features = pos.features.join(', ');
    }

    return pos;
}

function buildWorkingHoursString(daysObj) {
    const daysObjKeys = Object.keys(daysObj);
    let cell = '';
    daysObjKeys.forEach((day, index) => {
        cell+=`${day}: ${daysObj[day]}`;
        if (index < daysObjKeys.length-1){
            cell+= ' | ';
        }
    })

    return cell;
}

module.exports = posService;

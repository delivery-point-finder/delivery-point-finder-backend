const sessionCache = {};

const sessionService = {
    insertUser(token, userId) {
        removeOldUserToken(userId);
        sessionCache[token] = { _id: userId  };
    },
    findTokenInfo(token) {
        return sessionCache[token];
    },
    removeUserToken(token) {
        delete sessionCache[token];
        return true;
    }
};

function removeOldUserToken(userId) {
    const oldUserToken = Object.keys(sessionCache).find((token) => {
        return sessionCache[token]._id === userId;
    });

    if (oldUserToken) {
        delete sessionCache[oldUserToken];
    }
}

module.exports = sessionService;

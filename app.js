const express = require('express');
const cors = require('cors')
const createError = require('http-errors');
const logger = require('morgan');

const mongoose = require('./config/mongoose');
const passport = require('./config/passport');

const routes = require('./routes');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

try {
    mongoose();
    passport();
} catch (error) {
    console.log(error);
    process.exit(1);
}

app.use(cors({ origin: '*' }));
app.use(routes);

app.use((req, res, next) => {
    next(createError(404));
});

app.use((err, req, res, next) => {
    const {message, status} = err;

    res.status(status || 500).send({message});
});

module.exports = app;

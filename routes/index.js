const express = require('express');
const router = express.Router();

const posRouter = require('./pos');
const loginRouter = require('./login');
const usersRouter = require('./users');

router.use('/pos', posRouter);
router.use('/auth', loginRouter);
router.use('/users', usersRouter);

module.exports = router;

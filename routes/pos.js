const express = require('express');
const passport = require('passport');
const router = express.Router();
const fs = require('fs');
const multer = require('multer');
const posController = require('../controllers/pos.controller');

let tempDir = '/tmp/';

function getFileExtension(fileName) {
    return fileName.split('.').pop();
}

const storage = multer.diskStorage({
    destination(req, file, cb) {

        try {
            fs.accessSync(`${tempDir}${file.fieldname}/`, fs.constants.R_OK);
        } catch (err) {
            fs.mkdirSync(`${tempDir}${file.fieldname}/`);
        }

        cb(null, `${tempDir}${file.fieldname}/`);
    },
    filename(req, file, cb) {
        cb(null, `${Date.now()}.${getFileExtension(file.originalname)}`);
    },
});

const upload = multer({ storage });

const cepsListFile = upload.single('cepsList');

router.get('/from-cep/:cep', passport.authenticate('bearer', { session: false }), posController.getNearPosByCEP);
router.post('/from-cep-list', passport.authenticate('bearer', { session: false }), cepsListFile, posController.listOfNearPosByCEPs);

module.exports = router;

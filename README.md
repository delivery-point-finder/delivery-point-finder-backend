# Delivery Point Finder Backend

### Install
- git clone
- npm install
- set the env variables
- npm start

### Endpoints

#### **User's creation**

`POST /users`

Create users, does not need auth and only exists to ease the PoC.
```json
{
	"login": "aLogin",
	"password": "aPassword"
}
```
#### **Auth**

`POST /auth/login`

Make log in
```json
{
	"login": "aLogin",
	"password": "aPassword"
}
```
Returns a token
```json
{
	"token": "aReallyBigToken"
}
```

`POST /auth/logout`

Makes the logout using the token that is available in the Authorization Header.

`GET /auth/check`

Returns 200 if logged in or 401 if not authorized. It uses the token that is available in the Authorization Header.

#### **Points of delivery**

These endpoints needs Bearer Authentication.

`GET /pos/from-cep/:cep`

You must send a CEP(`:cep`) with the URL.

Returns the 10 nearest points of delivery from CEP
```json
{
    "results": [
        {
            "id": "1639",
            "nome_fantasia": "CALITO MOVEIS & ELETRODOMESTICOS",
            "endereco": "Rua nossa senhora do Rosário",
            "numero": "559",
            "complemento": "",
            "bairro": "Nossa Senhora da Paz",
            "cep": "88380000",
            "cidade": "Balneário Piçarras",
            "estado": "SC",
            "telefone": "47988180580",
            "latitude": "-27.58353720",
            "longitude": "-48.59542040",
            "foto": "https://imagemcliente.s3.sa-east-1.amazonaws.com/f764b21cac8b0fb68dbedaa4cbd49a0c.png",
            "distancia": "0,77",
            "horario_funcionamento": {
                "segunda": "08:30 - 17:30",
                "terca": "08:30 - 17:30",
                "quarta": "08:30 - 17:30",
                "quinta": "08:30 - 17:30",
                "sexta": "08:30 - 17:30",
                "sabado": "08:30 - 12:00",
                "almoco": " - "
            },
            "features": [
                "Estacionamento",
                "Horário estendido"
            ]
        }
    ],
    "error": false
}
```

`POST /pos/from-cep-list`
Receives a csv file with a lists of CEPs like:
```
12345678\r\n
12345678\r\n
12345678\r\n
12345678\r\n
12345678\r\n
```

Returns a csv file with the information of the 10 nearest points of delivery for each valid CEP


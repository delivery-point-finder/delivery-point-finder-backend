const authenticationService = require('../services/authentication.service');
const authorizationService = require('../services/authorization.service');

const loginController = {
    login(req, res, next) {
        const { login, password } = req.body;

        authenticationService.login(login, password)
            .then((token) => {
                res.status(200).send(token);
            })
            .catch(next);
    },
    logout(req, res, next) {
        const token = req.headers.authorization ? req.headers.authorization.split(' ')[1] : null;

        try {
            res.status(200).send(authenticationService.logout(token));
        } catch (e) {
            next(e);
        }
    },
    authCheck(req, res, next) {
        const token = req.headers.authorization ? req.headers.authorization.split(' ')[1] : null;

        if(!authorizationService.findTokenInfo(token)) {
            const error = new Error();
            error.status = 401;
            error.message = 'Não autorizado';

            throw error;
        }

        res.status(200).send(true);
    }
};

module.exports = loginController;

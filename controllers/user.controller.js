const userService = require('../services/user.service');

const userController = {
    createUser(req, res, next) {
        const fields = req.body;

        const user = {
            login: fields.login,
            password: fields.password,
        };

        userService.createUser(user)
            .then((userSaved) => {
                res.status(200).send(userSaved);
            })
            .catch(next);
    },
};

module.exports = userController;

const stream = require('stream');
const posService = require('../services/pos.service');

const posController = {
    async getNearPosByCEP(req, res, next) {
        const cep = req.params.cep;

        posService.getNearPosByCEP(cep)
            .then((pos) => {
                res.status(200).send(pos);
            })
            .catch(next);
    },
    async listOfNearPosByCEPs(req, res, next) {
        const cepsList = req.file;

        posService.listOfNearPosByCEPs(cepsList)
            .then((csv) => {
                const readStream = new stream.PassThrough();
                readStream.end(csv);

                res.header('Content-Type', 'application/octet-stream');
                res.header('Content-disposition', 'attachment; filename=list-of-near-pos-by-cep.csv');

                res.status(200);
                readStream.pipe(res);
            })
            .catch(next);
    }
}

module.exports = posController;

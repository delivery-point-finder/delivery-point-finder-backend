const path = require('path');

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config({ path: path.resolve(__filename, '../../.env') });
}

class Env {
    static get NODE_ENV() {
        return process.env.NODE_ENV || 'development';
    }

    static get MONGO_URL() {
        return process.env.MONGO_URL;
    }

    static get PEGAKI_API_KEY() {
        return process.env.PEGAKI_API_KEY || 'key';
    }

    static get PEGAKI_API_URL() {
        return process.env.PEGAKI_API_URL || 'url';
    }

    static get SECRET() {
        return process.env.SECRET || 'hash';
    }
}

module.exports = Env;
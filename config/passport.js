const passport = require('passport');
const BearerStrategy = require('passport-http-bearer');

const authorizationService = require('../services/authorization.service');

module.exports = () => passport.use(new BearerStrategy((token, done) => {
    if(!authorizationService.findTokenInfo(token)) {
        const err = new Error('Não autorizado');
        err.status = 401;

        return done(err, false);
    }

    return done(null, authorizationService.findTokenInfo(token));
}));
